from core.utils import is_int
from core.bot import XenialClient
from discord.ext import commands
from datetime import datetime

import minecraft_data
import asyncio
import typing
import discord
import json
import aiofiles
import random
import string


class Fun(commands.Cog):
    """Some fun commands to spend your time with."""

    def __init__(self, bot):
        self.bot: XenialClient = bot
        self.mc_data = minecraft_data("1.17")
        self.loop: asyncio.BaseEventLoop = bot.loop
        self._cog_emoji = "🎉"

    async def _get_item_or_block(self, name: str):
        return await self.loop.run_in_executor(None, self.mc_data.find_item_or_block, name)

    @commands.command()
    @commands.cooldown(1, 3, commands.BucketType.user)
    async def minecraft(self, ctx: commands.Context, *, name: typing.Optional[str] = None):
        """Returns information about an item/block in Minecraft. Currently gets info about Minecraft 1.17"""
        if not name:
            return await ctx.send("Please enter the name a block or item at the end of the command.")
        data = await self._get_item_or_block(name)

        if not data:
            return await ctx.send(f"`{name}` is an invalid item/block name. Example name of an item: `diamond_sword`")

        em = discord.Embed(title=data["displayName"], color=discord.Color.blue())
        em.add_field(name="Minecraft ID", value=f"{data['name']}", inline=True)
        em.add_field(name="Stack Size", value=data["stackSize"], inline=True)

        is_item = True if "durability" in data else False

        if is_item:
            em.add_field(
                name="Enchantment Categories",
                value="\n".join([f"- {x.capitalize()}" for x in data["enchantCategories"]]),
                inline=False,
            )
            em.add_field(
                name="Fixed with",
                value="\n".join([f"- {x.capitalize()}" for x in data["fixedWith"]]),
                inline=False,
            )
            em.add_field(name="Durability", value=data["durability"], inline=True)
            em.add_field(name="Maximum Durability", value=data["maxDurability"])

        em.timestamp = datetime.now().astimezone()
        em.set_footer(text=self.bot.user, icon_url=self.bot.user.avatar_url)

        return await ctx.send(embed=em)

    @commands.command()
    @commands.cooldown(1, 3, commands.BucketType.user)
    async def meme(self, ctx: commands.Context):
        """Finds a meme for you on Reddit"""
        async with self.bot.session.get("https://meme-api.herokuapp.com/gimme") as response:
            r = json.loads(await response.text())

        if r["nsfw"]:
            return await ctx.send(
                "When I tried to get a good meme for you, I received an NSFW meme instead. Please run this command again."
            )

        em = discord.Embed(title=r["title"], color=discord.Color.orange(), url=r["postLink"])
        em.set_image(url=r["url"])
        em.set_footer(text=f"👍 {r['ups']} | Requested by: {ctx.message.author}")
        em.set_author(name=r["author"])
        em.timestamp = datetime.now().astimezone()

        return await ctx.send(embed=em)

    @commands.command()
    @commands.cooldown(1, 3, commands.BucketType.user)
    async def joke(self, ctx: commands.Context):
        """Cracks a joke for you"""
        async with self.bot.session.get("https://official-joke-api.appspot.com/jokes/random") as response:
            r = json.loads(await response.text())

        em = discord.Embed(
            title="Joke",
            color=discord.Color.green(),
            description=f"{r['setup']}\n" + f"||{r['punchline']}||",
        )
        em.set_footer(text=f"Requested by: {ctx.message.author}")
        em.timestamp = datetime.now().astimezone()

        await ctx.send(embed=em)

    @commands.command()
    @commands.cooldown(1, 5, commands.BucketType.user)
    async def urban(self, ctx: commands.Context, *, query: typing.Optional[str]):
        """Searches a word for you on the Urban dictionary"""
        if not query:
            return await ctx.send("Please enter your search query at the end of the command")

        async with self.bot.session.get(f"https://api.urbandictionary.com/v0/define?term={query}") as response:
            data = json.loads(await response.text())

        if not len(data["list"]):
            return await ctx.send("Couldn't find your search in the dictionary...")

        result = data["list"][0]
        definition = result["definition"]

        if len(definition) >= 1000:
            definition = definition[:1000]
            definition = definition.rsplit(" ", 1)[0]
            definition += "..."

        em = discord.Embed(
            title=f"Urban definition for {result['word']}",
            color=discord.Color.magenta(),
        )
        em.url = result["permalink"]
        em.add_field(name="Definition", value=result["definition"], inline=False)
        em.add_field(name="Example", value=result["example"])
        em.add_field(
            name="Info",
            value=f"**Author:** {result['author']}\n**Written on:** "
            + datetime.strptime(result["written_on"], "%Y-%m-%dT%H:%M:%S.%fZ").strftime("%d %b, %Y on %I:%M:%S %p"),
        )
        em.set_footer(text=f"👍 {result['thumbs_up']} | 👎 {result['thumbs_down']}")
        em.timestamp = datetime.now().astimezone()

        await ctx.send(embed=em)

    @commands.command(name="8ball", aliases=["fortune"])
    async def _8ball(self, ctx: commands.Context, *, question: typing.Optional[str]):
        """Tells your fortune or answers a yes/no question"""
        if not question:
            return await ctx.send("Please enter your question at the end of the commnad")

        async with aiofiles.open(
            "assets/8ball.txt"
        ) as f:  # Since the cog is run from bot.py, we need it's relative dir
            content = await f.readlines()
            answer = random.choice(content)

        return await ctx.reply(answer)

    @commands.command()
    async def roast(self, ctx: commands.Context, *, person: typing.Optional[str]):
        """Roasts whoever you mention"""
        if not person:
            return await ctx.send("Mention someone to roast at the end of the command, or else I roast **you**")

        async with aiofiles.open(
            "assets/Roasts.txt"
        ) as f:  # Since the cog is run from bot.py, we need it's relative dir
            content = await f.readlines()
            response = random.choice(content)

        return await ctx.send(f"{person}, {response}")

    @commands.command()
    @commands.cooldown(1, 5, commands.BucketType.user)
    async def quote(self, ctx: commands.Context):
        """Randomly finds a quote by a famous author"""
        async with self.bot.session.get("https://api.quotable.io/random") as response:
            data = json.loads(await response.text())

        return await ctx.send(f"***{data['content']}\n***- *{data['author']}*")

    @commands.command()
    async def flip(self, ctx: commands.Context):
        """Flip a coin"""
        return await ctx.send(f"You have landed a **{'Heads' if random.choice([0, 1]) else 'Tails'}**!")

    @commands.command(aliases=["keygen", "passgen"])
    async def textgen(self, ctx: commands.Context, *, length: typing.Optional[str] = 12):
        """Generates some random text with letters and numbers"""
        if is_int(length):
            n = int(length)
            if n > 36:
                return await ctx.send("The length of the text cannot be more than 36")
        else:
            n = length

        return await ctx.send(
            f"Here's some random text for you: **{''.join(random.choices(string.ascii_uppercase + string.digits + string.ascii_lowercase, k=n))}**"
        )

    @commands.command()
    async def punch(self, ctx: commands.Context, *, person: typing.Optional[str]):
        """Punches whoever you mention, it hurts"""
        if not person:
            return await ctx.send("Mention someone to punch at the end of the command.")
        async with aiofiles.open(
            "assets/Punch.txt"
        ) as f:  # Since the cog is run from bot.py, we need it's relative dir
            content = await f.readlines()
            gif = random.choice(content)

        try:
            person = await commands.MemberConverter().convert(ctx, person)
        except Exception:
            pass

        em = discord.Embed(title=f"{ctx.author} punched {person}", color=discord.Color.green())
        em.timestamp = datetime.now().astimezone()
        em.set_image(url=gif)
        return await ctx.send(embed=em)


def setup(bot: XenialClient):
    bot.add_cog(Fun(bot))
