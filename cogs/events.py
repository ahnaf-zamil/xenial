from discord.ext import commands
from core.bot import XenialClient
from datetime import datetime

from db.models import GuildConfig

import discord
import typing
import traceback


class Events(commands.Cog):
    def __init__(self, bot):
        self.bot: XenialClient = bot

    async def _check_or_make_guild_config(self, guild: discord.Guild):
        """Creates guild config if it does not exist"""
        guild_config = await GuildConfig.filter(guild_id=str(guild.id)).get_or_none()

        if not guild_config:
            await self.bot.make_guild_config(guild)

    @staticmethod
    async def _remove_guild_config(guild: discord.Guild):
        guild_config = await GuildConfig.filter(guild_id=str(guild.id)).get_or_none()

        if guild_config:
            return await guild_config.delete()

    @staticmethod
    async def _inform_owner(
        bot: XenialClient,
        guild: discord.Guild,
        event: typing.Literal["join", "leave"] = "join",
    ):
        """Notifies bot owner that the bot has joined a guild"""
        app_info = await bot.application_info()
        dm = await app_info.owner.create_dm()

        if event == "join":
            em = discord.Embed(title="Joined guild", color=discord.Color.green())
        else:
            em = discord.Embed(title="Removed from guild", color=discord.Color.red())

        em.add_field(name="Name", value=guild.name)
        em.add_field(name="ID", value=guild.id)
        em.add_field(
            name="Created On",
            value=guild.created_at.strftime("%m/%d/%Y, %H:%M:%S"),
            inline=False,
        )
        em.add_field(name="Members", value=guild.member_count)
        em.add_field(name="Owner", value=guild.owner)
        em.set_thumbnail(url=guild.icon_url)
        em.timestamp = datetime.now().astimezone()

        return await dm.send(embed=em)

    @staticmethod
    async def _inform_owner_about_error(ctx: commands.Context, _traceback: str):
        """Notifies bot owner when an error occurs"""
        app_info = await ctx.bot.application_info()
        dm = await app_info.owner.create_dm()

        em = discord.Embed(title=f"Error in {ctx.guild} [{ctx.guild.id}]", color=discord.Color.dark_red())
        em.add_field(name=f"Author [{ctx.message.author.id}]", value=ctx.message.author, inline=False)
        em.add_field(name=f"Command Message [{ctx.message.id}]", value=ctx.message.content[:500], inline=False)
        em.add_field(name="Traceback", value=f"```\n{_traceback[:1500]}```", inline=False)
        em.timestamp = datetime.now().astimezone()

        return await dm.send(embed=em)

    @staticmethod
    async def _inform_author_about_error(ctx: commands.Context):
        """Tells the message author that an error occured"""
        em = discord.Embed(title="Error!!", color=discord.Color.red())
        em.description = "An error occured while running this command. The bot owner has been notified."
        em.timestamp = datetime.now().astimezone()

        return await ctx.reply(embed=em)

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        if message.content.strip() == f"<@!{self.bot.user.id}>":
            prefix = await self.bot.get_prefix(message)
            await message.reply(f"My prefix is `{prefix}`\nTo get a list of my commands, type `{prefix}help`")

    @commands.Cog.listener()
    async def on_guild_join(self, guild: discord.Guild):
        await self._check_or_make_guild_config(guild)
        await self._inform_owner(self.bot, guild)

    @commands.Cog.listener()
    async def on_guild_remove(self, guild: discord.Guild):
        await self._remove_guild_config(guild)
        await self._inform_owner(self.bot, guild, event="leave")

    @commands.Cog.listener()
    async def on_command_error(self, ctx: commands.Context, error):
        if isinstance(error, commands.CommandOnCooldown):
            em = discord.Embed(
                title="Command on cooldown",
                description=f"Slow down dude!\n\nYou can run this command again in **{error.retry_after:.2f}s**",
                color=discord.Color.red(),
            )
            em.timestamp = datetime.now().astimezone()
            return await ctx.send(embed=em)

        elif isinstance(error, commands.errors.MemberNotFound):
            return await ctx.send(f"Whoops! Member not found: **{error.argument}**.")

        elif isinstance(error, commands.errors.ChannelNotFound):
            return await ctx.send(f"Whoops! Text channel not found: **{error.argument}**.")
        elif isinstance(error, commands.errors.CommandNotFound) or isinstance(error, commands.errors.NotOwner):
            return await ctx.message.add_reaction("❓")

        elif isinstance(error, commands.errors.CommandInvokeError):
            formatted_traceback = "".join(traceback.format_exception(type(error), error, error.__traceback__))
            await self._inform_owner_about_error(ctx, formatted_traceback)
            await self._inform_author_about_error(ctx)

        return traceback.print_exception(type(error), error, error.__traceback__)


def setup(bot: XenialClient):
    bot.add_cog(Events(bot))
