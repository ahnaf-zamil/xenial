from discord.ext import commands
from core.bot import XenialClient

import json


class Animals(commands.Cog):
    """A category for pictures of different kinds of animals"""

    def __init__(self, bot: XenialClient):
        self.bot = bot
        self._cog_emoji = "🐶"

    @commands.command()
    @commands.cooldown(1, 5, commands.BucketType.user)
    async def dog(self, ctx: commands.Context):
        """Sends you a nice dog picture :D"""
        async with self.bot.session.get("https://dog.ceo/api/breeds/image/random") as response:
            data = json.loads(await response.text())
        return await ctx.reply(data["message"])

    @commands.command()
    @commands.cooldown(1, 5, commands.BucketType.user)
    async def cat(self, ctx: commands.Context):
        """Sends you a cute cat picture UwU"""
        async with self.bot.session.get("https://api.thecatapi.com/v1/images/search") as response:
            data = json.loads(await response.text())
        return await ctx.reply(data[0]["url"])

    @commands.command()
    @commands.cooldown(1, 5, commands.BucketType.user)
    async def bunny(self, ctx: commands.Context):
        """Sends you a picture of a funny bunny (it rhymes!)"""
        async with self.bot.session.get("https://api.bunnies.io/v2/loop/random/?media=gif") as response:
            data = json.loads(await response.text())
        return await ctx.reply(data["media"]["gif"])

    @commands.command()
    @commands.cooldown(1, 5, commands.BucketType.user)
    async def panda(self, ctx: commands.Context):
        """Sends you a picture of KUNG FU PANDA!!!!!!!!!!"""
        async with self.bot.session.get("https://some-random-api.ml/img/panda") as response:
            data = json.loads(await response.text())
        return await ctx.reply(data["link"])

    @commands.command(aliases=["birb"])
    @commands.cooldown(1, 5, commands.BucketType.user)
    async def bird(self, ctx: commands.Context):
        """Sends you a picture of an angry bird"""
        async with self.bot.session.get("https://some-random-api.ml/img/birb") as response:
            data = json.loads(await response.text())
        return await ctx.reply(data["link"])


def setup(bot: XenialClient):
    bot.add_cog(Animals(bot))
