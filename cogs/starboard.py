from db.models import StarboardConfig, StarredMessages
from discord.ext import commands
from core.bot import XenialClient

from db import GuildConfig

import discord
import asyncio
import typing
import datetime


class Starboard(commands.Cog):
    """Configure starboard for this server."""

    def __init__(self, bot):
        self.bot: XenialClient = bot
        self._cog_emoji = "⭐"

    async def _add_to_starboard(self, starboard_config: StarboardConfig, message: discord.Message, stars: int):
        # Sending msg to starboard channel
        starboard_channel = discord.utils.get(message.guild.text_channels, id=starboard_config.channel_id)
        embed = discord.Embed(
            title="Jump to message!",
            url=message.jump_url,
            description=message.content,
            color=discord.Color.gold(),
        )
        embed.set_author(name=str(message.author), icon_url=message.author.avatar_url)
        if len(message.attachments) > 0:
            embed.set_image(url=message.attachments[0].url)

        embed.timestamp = datetime.datetime.astimezone()
        embed.set_footer(text=f"Message ID: {message.id}")

        starboard_msg = await starboard_channel.send(f":star: **{stars}** | {message.channel.mention}", embed=embed)
        # Adding to DB
        return await StarredMessages.create(
            message_id=message.id,
            channel_id=message.channel.id,
            starboard_message_id=starboard_msg.id,
            stars=stars,
        )

    async def _remove_from_starboard(
        self,
        starboard_config: StarboardConfig,
        starred_msg: StarredMessages,
        message: discord.Message,
    ):
        # Deleting from DB
        await starred_msg.delete()

        # Deleting starboard message
        starboard_channel: typing.Optional[discord.TextChannel] = discord.utils.get(
            message.guild.text_channels, id=starboard_config.channel_id
        )
        _starred_msg: discord.Message = await starboard_channel.fetch_message(starred_msg.starboard_message_id)
        return await _starred_msg.delete()

    async def _update_starboard(
        self,
        starboard_config: StarboardConfig,
        starred_msg: StarredMessages,
        message: discord.Message,
        stars: int,
    ):
        # Updating DB
        starred_msg.stars = stars
        await starred_msg.save()

        starboard_channel: typing.Optional[discord.TextChannel] = discord.utils.get(
            message.guild.text_channels, id=starboard_config.channel_id
        )

        # Updating starboard msg
        msg: discord.Message = await starboard_channel.fetch_message(starred_msg.starboard_message_id)
        starred_msg_channel: typing.Optional[discord.TextChannel] = discord.utils.get(
            msg.guild.text_channels, id=starred_msg.channel_id
        )
        return await msg.edit(content=f":star: **{stars}** | {starred_msg_channel.mention}")

    async def _ask_starboard_channel(self, ctx: commands.Context):
        try:
            msg: discord.Message = await self.bot.wait_for(
                "message",
                check=XenialClient.check_message_author(ctx.author),
                timeout=20,
            )
            return await commands.TextChannelConverter().convert(ctx, msg.content)
        except commands.errors.ChannelNotFound as e:
            # Recursive, will ask for input again if user gives invalid input
            await ctx.send(f"Invalid channel `{e.argument}`. Please enter a channel name again.")
            return await self._ask_starboard_channel(ctx)

    async def _ask_star_requirement(self, ctx: commands.Context):
        try:
            msg: discord.Message = await self.bot.wait_for(
                "message",
                check=XenialClient.check_message_author(ctx.author),
                timeout=20,
            )
            stars = int(msg.content)
            if (
                stars > ctx.guild.member_count - 1
            ):  # Subtracting one since the author cannot react to their own msg with a star
                await ctx.reply(
                    "The number of stars entered is more than the number of members in this guild. Please enter the required star amount again"
                )
                return await self._ask_star_requirement(ctx)
            return stars

        except ValueError:
            # Recursive, will ask for input again if user gives invalid input
            await ctx.reply("Invalid input, please enter the required star amount again.")
            return await self._ask_star_requirement(ctx)

    async def _enable_starboard(self, ctx: commands.Context, guild_config: GuildConfig):
        try:
            starboard_config = {
                "guild_id": ctx.guild.id,
            }

            await ctx.reply("Please mention the channel you want to configure starboard for")
            starboard_config["channel_id"] = (await self._ask_starboard_channel(ctx)).id

            await ctx.reply(
                "Please enter the amount of :star: you want for a message to be sent to starboard. Default is **3**."
            )
            starboard_config["star_requirement"] = await self._ask_star_requirement(ctx)

            await StarboardConfig.create(**starboard_config)
            guild_config.starboard_enabled = True
            await guild_config.save()

            return await ctx.send(
                f"""Congratulations! Starboard has now been enabled for this guild.
All messages with at least **{starboard_config['star_requirement']}** :star: will be sent to <#{starboard_config['channel_id']}>"""
            )

        except asyncio.TimeoutError:
            prefix = await self.bot.get_prefix(ctx.guild)
            return await ctx.reply(
                f"Your response timed out. Please run `{prefix}starboard enable` again if you want to enable starboard."
            )

    async def _configure_stars(self, ctx: commands.Context):
        starboard_config = await StarboardConfig.filter(guild_id=ctx.guild.id).get_or_none()
        await ctx.reply(
            f"Please enter the amount of :star: you want for a message to be sent to starboard. Currently, it is set to **{starboard_config.star_requirement}** :star:."
        )
        star_count = await self._ask_star_requirement(ctx)
        starboard_config.star_requirement = star_count
        await starboard_config.save()

        return await ctx.send(
            f"The amount of :star: required for a message to be sent to starboard is now set to **{star_count}**."
        )

    async def _configure_channel(self, ctx: commands.Context):
        starboard_config = await StarboardConfig.filter(guild_id=ctx.guild.id).get_or_none()
        await ctx.reply(
            f"Please mention the channel you want to configure starboard for. Currently, it is set to <#{starboard_config.channel_id}>."
        )
        channel = await self._ask_starboard_channel(ctx)
        starboard_config.channel_id = channel.id
        await starboard_config.save()

        return await ctx.send(f"The starboard channel has now been set to {channel.mention}.")

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, event: discord.RawReactionActionEvent):
        emoji: discord.PartialEmoji = event.emoji

        if emoji.is_unicode_emoji() and str(emoji) == self._cog_emoji:
            starboard_config = await StarboardConfig.filter(guild_id=event.guild_id).get_or_none()

            if not starboard_config:
                return

            channel: typing.Optional[discord.TextChannel] = self.bot.get_channel(event.channel_id)
            message: discord.Message = await channel.fetch_message(event.message_id)

            if message.author.id == event.user_id or message.author.bot:
                return await message.remove_reaction(self._cog_emoji, event.member)

            star_count = 0

            for x in message.reactions:
                if x.emoji == self._cog_emoji:
                    star_count = x.count

            if star_count >= starboard_config.star_requirement:
                starred_message = await StarredMessages.filter(message_id=message.id).get_or_none()
                if starred_message:
                    return await self._update_starboard(
                        starboard_config,
                        starred_message,
                        message,
                        starred_message.stars + 1,
                    )
                else:
                    return await self._add_to_starboard(starboard_config, message, star_count)

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, event: discord.RawReactionActionEvent):
        emoji: discord.PartialEmoji = event.emoji

        if emoji.is_unicode_emoji() and str(emoji) == self._cog_emoji:
            starboard_config = await StarboardConfig.filter(guild_id=event.guild_id).get_or_none()

            if not starboard_config:
                return

            channel: typing.Optional[discord.TextChannel] = self.bot.get_channel(event.channel_id)
            message: discord.Message = await channel.fetch_message(event.message_id)

            star_count = 0
            for x in message.reactions:
                if x.emoji == self._cog_emoji:
                    star_count = x.count

            starred_message = await StarredMessages.filter(message_id=message.id).get_or_none()

            if not starred_message:
                return

            if starboard_config.star_requirement > star_count:
                # Removes msg from starboard if star count falls below the requirement
                return await self._remove_from_starboard(starboard_config, starred_message, message)

            return await self._update_starboard(starboard_config, starred_message, message, star_count)

    @commands.group(invoke_without_command=True)
    @XenialClient.has_guild_config()
    async def starboard(self, ctx: commands.Context):
        """Shows you the current status of starboard for this guild"""
        guild_config = await GuildConfig.filter(guild_id=ctx.guild.id).get_or_none()
        if not guild_config.starboard_enabled:
            prefix = await self.bot.get_prefix(ctx.guild)
            return await ctx.send(
                f"Starboard is not enabled for this guild. To enable starboard, run `{prefix}starboard enable`."
            )
        starboard_config = await StarboardConfig.filter(guild_id=ctx.guild.id).get_or_none()

        return await ctx.send(
            f"Starboard has been enabled for this guild. All messages with at least **{starboard_config.star_requirement}** :star: will be sent to <#{starboard_config.channel_id}>"
        )

    @commands.has_guild_permissions(manage_guild=True)
    @starboard.command()
    @XenialClient.has_guild_config()
    async def enable(self, ctx: commands.Context):
        """Enables starboard for this guild"""
        guild_config = await GuildConfig.filter(guild_id=ctx.guild.id).get_or_none()
        if not guild_config.starboard_enabled:
            return await self._enable_starboard(ctx, guild_config)
        return await ctx.send("Starboard has already been enabled for this guild.")

    @commands.has_guild_permissions(manage_guild=True)
    @starboard.command()
    @XenialClient.has_guild_config()
    async def disable(self, ctx: commands.Context):
        """Disables starboard and removes config"""
        guild_config = await GuildConfig.filter(guild_id=ctx.guild.id).get_or_none()
        if not guild_config.starboard_enabled:
            return await ctx.send("Starboard has not been enabled for this guild yet.")

        starboard_config = await StarboardConfig.filter(guild_id=ctx.guild.id).get_or_none()
        await starboard_config.delete()
        guild_config.starboard_enabled = False
        await guild_config.save()
        return await ctx.send("Disabled starboard for this guild, and deleted the config.")

    @commands.has_guild_permissions(manage_guild=True)
    @starboard.command(aliases=["config", "cfg"])
    @XenialClient.has_guild_config()
    async def configure(self, ctx: commands.Context, *, setting: typing.Optional[str] = None):
        """Change the starboard configuration for this guild"""
        settings = ["stars", "channel"]

        if not setting:
            prefix = await self.bot.get_prefix(ctx.guild)
            return await ctx.send(
                f"""Please enter the setting which you configure at the end of the command. Available settings are {', and'.join(map(lambda x: '`{}`'.format(x), settings))}.
Example: `{prefix}starboard configure stars`."""
            )

        if setting not in settings:
            return await ctx.send(
                f"Invalid setting `{setting}`. Available settings are {', and'.join(map(lambda x: '`{}`'.format(x), settings))}"
            )

        guild_config = await GuildConfig.filter(guild_id=ctx.guild.id).get_or_none()
        if not guild_config.starboard_enabled:
            return await ctx.send("Starboard has not been enabled for this guild yet.")

        if setting == "stars":
            return await self._configure_stars(ctx)
        else:
            return await self._configure_channel(ctx)


def setup(bot: XenialClient):
    bot.add_cog(Starboard(bot))
