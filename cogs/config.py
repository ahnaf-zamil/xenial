from discord.ext import commands
from core.bot import XenialClient

import typing


class Config(commands.Cog):
    """Customize Xenial for this server."""

    def __init__(self, bot):
        self.bot: XenialClient = bot
        self._cog_emoji = "⚙️"

    @commands.has_guild_permissions(manage_guild=True)
    @commands.command()
    async def prefix(self, ctx: commands.Context, *, prefix: typing.Optional[str] = None):
        """Sets/Returns the bot's prefix for this server"""
        if prefix and prefix.strip():
            await self.bot.redis.set(str(ctx.guild.id), prefix)
            return await ctx.send(f"Set my prefix to {prefix}")

        prefix = (await self.bot.redis.get(str(ctx.guild.id))).decode()
        return await ctx.send(f"My current prefix is: **{prefix}**")


def setup(bot: XenialClient):
    bot.add_cog(Config(bot))
