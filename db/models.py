from tortoise.models import Model
from tortoise import fields


class GuildConfig(Model):
    guild_id = fields.BigIntField(pk=True, unique=True, generated=False, null=False)  # Will add more stuff later on
    starboard_enabled = fields.BooleanField(default=False, null=False)


class StarboardConfig(Model):
    guild_id = fields.BigIntField(pk=True, unique=True, generated=False, null=False)
    star_requirement = fields.IntField(null=False)
    channel_id = fields.BigIntField(unique=True, null=False)


class StarredMessages(Model):
    message_id = fields.BigIntField(pk=True, unique=True, generated=False, null=False)
    channel_id = fields.BigIntField(unique=True, null=False)
    starboard_message_id = fields.BigIntField(unique=True, null=False)
    stars = fields.IntField(null=False)
