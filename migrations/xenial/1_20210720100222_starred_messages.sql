-- upgrade --
ALTER TABLE "starboardconfig" RENAME COLUMN "id" TO "guild_id";
ALTER TABLE "starboardconfig" DROP COLUMN "guild_id";
CREATE TABLE IF NOT EXISTS "starredmessages" (
    "message_id" BIGINT NOT NULL  PRIMARY KEY,
    "channel_id" BIGINT NOT NULL UNIQUE,
    "starboard_message_id" BIGINT NOT NULL UNIQUE,
    "stars" INT NOT NULL
);;
-- downgrade --
ALTER TABLE "starboardconfig" RENAME COLUMN "guild_id" TO "id";
ALTER TABLE "starboardconfig" ADD "guild_id" BIGINT NOT NULL UNIQUE;
DROP TABLE IF EXISTS "starredmessages";
