-- upgrade --
CREATE TABLE IF NOT EXISTS "guildconfig" (
    "guild_id" BIGINT NOT NULL  PRIMARY KEY,
    "starboard_enabled" BOOL NOT NULL  DEFAULT False
);
CREATE TABLE IF NOT EXISTS "starboardconfig" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "guild_id" BIGINT NOT NULL UNIQUE,
    "star_requirement" INT NOT NULL,
    "channel_id" BIGINT NOT NULL UNIQUE
);
CREATE TABLE IF NOT EXISTS "aerich" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "version" VARCHAR(255) NOT NULL,
    "app" VARCHAR(20) NOT NULL,
    "content" JSONB NOT NULL
);
