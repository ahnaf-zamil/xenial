from core.bot import XenialClient

import discord
import colorama
import _about


def main():
    colorama.init()

    client = XenialClient()

    # Requires discord.py 1.6.0
    if discord.__version__ != "1.6.0":
        raise ValueError(f"{_about.__name__} requires Discord.py v1.6.0. You are now running v{discord.__version__}")

    client.run()


if __name__ == "__main__":
    main()
