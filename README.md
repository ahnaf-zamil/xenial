<img width="120" height="120" style="float: left; margin-right: 20px;" alt="Xenial" src="https://images-ext-2.discordapp.net/external/d9_ss5NMfr6i6vfo3FyJxYQVgt82sVaub2OW1CVL4OY/%3Fsize%3D1024/https/cdn.discordapp.com/avatars/865548638020829206/110094a6855714566e172ac41265d6c4.webp?width=676&height=676">

# Xenial

Xenial (pronounced Ex-enial) is a Discord bot written in Python that is designed for suiting your server's needs.

<br/>

[![License: Apache-2.0](https://img.shields.io/badge/license-Apache--2.0-orange?style=flat-square)](https://opensource.org/licenses/Apache-2.0)
[![Code Style: Black](https://img.shields.io/badge/Code%20Style-Black-black?style=flat-square)](https://gitlab.com/psf/black)
[![pyvers](https://img.shields.io/badge/python-3.8%20%7C%203.9-green?style=flat-square)]()
[![Lines of code](https://tokei.rs/b1/gitlab/ahnaf-zamil/xenial?style=flat-square)](https://gitlab.com/ahnaf-zamil/xenial)

## Commands & Features

-   **📖 Info**: `ping`, `serverinfo`, `userinfo`, `channelinfo`, `servericon`, `avatar`
-   **🎉 Fun**: `minecraft`, `meme`, `joke`, `urban`, `8ball`, `roast`, `quote`, `flip`, `textgen`, `punch`
-   **⭐ Starboard**: `starboard`, `starboard enable`, `starboard disable`, `starboard configure`
-   **⚙️ Config**: `prefix`
-   **🐶 Animals**: `dog`, `cat`, `bunny`, `panda`, `bird`

## Status

Currently, this bot is a work in progress. But it will be up and running in no time. I plan on hosting it soon once it has a few more features. To speed up the process, consider contributing, it will be **really** appreciated ^-^

## Running it locally

We recommend you use [Docker](https://docker.com) for this as it simplifies the process **a lot**.

Clone the repo:

```bash
$ git clone https://gitlab.com/ahnaf-zamil/xenial
$ cd xenial
```

Now fill in the `.env` according to the `.env.example` file. You can also modify `config.json` if you want to.

Running the bot:

```bash
$ docker-compose up -d
```

Checking logs:

```bash
$ docker logs xenial-bot
```

Stopping bot:

```bash
$ docker-compose down
```

## Contribution

Contributing to the development of Xenial will be really appreciated. It will help you show your skills as a bot dev, as well as make this a better bot. Also, you will be helping me out, so I would be very happy :D

## License

Xenial is released under the [Apache 2.0 License](https://www.apache.org/licenses/LICENSE-2.0). Check it out [here](LICENSE).
