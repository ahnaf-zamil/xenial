from core.utils import Uptime
from tortoise import Tortoise
from discord.ext import commands
from dotenv import load_dotenv
from datetime import datetime
from db import GuildConfig
from .help import HelpCommand

import discord
import json
import os
import logging
import sys
import asyncio
import typing
import aioredis
import aiohttp

import _about


load_dotenv()  # Loading dotenv, just in case someone doesn't run it with Docker

if os.name == "nt":
    # Setting event loop policy since asyncio crashes with RuntimeError: Event loop is closed, mostly because of aiohttp
    # https://github.com/aio-libs/aiohttp/issues/4324
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())


TORTOISE_ORM_CONFIG = {
    "connections": {
        "default": f"postgres://{os.environ['POSTGRES_USER']}:{os.environ['POSTGRES_PASSWORD']}@{os.environ['POSTGRES_HOST']}:5432/{os.environ['POSTGRES_DB']}"
    },
    "apps": {
        "xenial": {
            "models": ["db.models", "aerich.models"],
            "default_connection": "default",
        },
    },
}


class XenialClient(commands.AutoShardedBot):
    def __init__(self, intents=discord.Intents.all(), logger=None):
        super().__init__(
            command_prefix=self.get_prefix,
            intents=intents,
            help_command=HelpCommand(),
            case_insensitive=True,
        )
        self._logger: typing.Optional[logging.Logger] = logger

        self._redis_client = None
        self._session = self.loop.run_until_complete(self._create_session())

        self._is_ready = False
        self._db_initialized = False
        self.start_time = None

    # Properties
    @property
    def token(self) -> str:
        return os.environ["TOKEN"]

    @property
    def logger(self) -> logging.Logger:
        if not self._logger:
            self._logger = logging.getLogger("xenial")
        return self._logger

    @property
    def config(self) -> dict:
        with open("./config.json") as f:
            return json.load(f)

    @property
    def redis(self) -> typing.Optional[aioredis.Redis]:
        return self._redis_client

    @property
    def session(self) -> aiohttp.ClientSession:
        return self._session

    @property
    def uptime(self) -> datetime:
        delta_uptime = datetime.utcnow() - self.start_time
        hours, remainder = divmod(int(delta_uptime.total_seconds()), 3600)
        minutes, seconds = divmod(remainder, 60)
        days, hours = divmod(hours, 24)

        return Uptime(days=days, hours=hours, minutes=minutes, seconds=seconds)

    @property
    def guild_count(self) -> int:
        return len(self.guilds)

    async def on_ready(self):
        self.start_time = datetime.utcnow()
        if not self._redis_client:
            self._redis_client = await aioredis.create_redis_pool(os.environ["REDIS_URI"])
            self.logger.info(f"Connected to Redis on {':'.join(str(x) for x in self._redis_client.address)}")

        if not self._db_initialized:
            await self.init_db()
            self._db_initialized = True

        await self.change_presence(
            activity=discord.Activity(type=discord.ActivityType.watching, name=f"over {self.guild_count} servers")
        )

        self.logger.info("Bot is ready")
        self.logger.info(f"Logged in as {self.user}")
        self.logger.info(f"ID: {self.user.id}")
        self.logger.info(f"Guild count: {len(self.guilds)}")

        self._is_ready = True

    async def get_prefix(self, arg: typing.Union[discord.Message, discord.Guild]):
        """Returns the bot's prefix for a specific server from Redis, or the default prefix if it doesn't exist"""
        if isinstance(arg, discord.Message):
            prefix = await self.redis.get(str(arg.guild.id))
        else:
            prefix = await self.redis.get(str(arg.id))

        if prefix:
            return prefix.decode()
        return self.config["default_prefix"]

    async def on_message(self, message: discord.Message):
        if self._is_ready:
            # If the bot is ready (all db connections have been initialized, etc)
            return await self.process_commands(message)

    async def init_db(self):
        await Tortoise.init(config=TORTOISE_ORM_CONFIG)
        await Tortoise.generate_schemas()
        self.logger.info("Initialized database connection.")
        self.logger.info("")

    @classmethod
    def has_guild_config(cls):
        """Custom check which creates a guild config if it doesn't already exist"""

        async def predicate(ctx: commands.Context):
            guild_config = await GuildConfig.filter(guild_id=str(ctx.guild.id)).get_or_none()
            if not guild_config:
                await cls.make_guild_config(ctx.guild)
            return True

        return commands.check(predicate)

    @staticmethod
    def check_message_author(author: discord.User):
        """A check used for checking whether or not the message author
        is the command author, in case of wait_for message"""

        def inner(message: discord.Message):
            return message.author.id == author.id

        return inner

    @staticmethod
    async def make_guild_config(guild: discord.Guild):
        config = {"guild_id": str(guild.id)}
        return await GuildConfig.create(**config)

    async def _create_session(self):
        return aiohttp.ClientSession()

    def _load_cogs(self):
        cogs = self.config["cogs"]

        for cog in cogs:
            self.logger.info(f"Loaded cog: {cog}")
            self.load_extension(f"cogs.{cog}")

    def _display_banner(self):
        banner_text = f"""
__   __           _       _
\ \ / /          (_)     | |
 \ V /  ___ _ __  _  __ _| |
 /   \ / _ \ '_ \| |/ _` | |
/ /^\ \  __/ | | | | (_| | |
\/   \/\___|_| |_|_|\__,_|_|

Version: {_about.__version__}
Author: {_about.__author__}
        """
        for line in banner_text.split("\n"):
            self.logger.info(line)

    def _setup_logging(self):
        # Creating logs dir if it doesn't exist
        if not os.path.exists("logs"):
            os.makedirs("logs")

        logger = self.logger
        log_dir = os.path.join(os.getcwd(), "logs")
        log_path = os.path.join(log_dir, f"{datetime.now().strftime('%m-%d-%y %H-%M-%S')}.log")

        formatter = logging.Formatter(
            fmt="%(asctime)s %(name)s - %(levelname)s: %(message)s",
            datefmt="%m/%d/%y %H:%M:%S",
        )
        screen_handler = logging.StreamHandler(stream=sys.stdout)
        screen_handler.setFormatter(formatter)

        file_handler = logging.FileHandler(log_path, mode="w")
        file_handler.setFormatter(formatter)

        logger.addHandler(screen_handler)
        logger.addHandler(file_handler)
        logger.setLevel(logging.INFO)

    async def close(self):
        self.logger.info("Closing bot...")
        await self._session.close()
        await Tortoise.close_connections()
        await super().close()

    def run(self):
        self._setup_logging()
        try:
            # pyright: reportMissingImports=false
            import uvloop

            self.logger.info("Using uvloop instead of the built-in asyncio event loop")
            uvloop.install()
        except ImportError:
            pass

        self._load_cogs()

        try:
            self._display_banner()
            super().run(self.token)
        except discord.PrivilegedIntentsRequired:
            self.logger.critical(
                f"{_about.__name__} requires privileged intents, which have not been enabled in the Discord portal."
            )
            sys.exit(1)
