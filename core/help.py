from discord.ext import commands
from datetime import datetime

import typing
import discord
import DiscordUtils


class HelpCommand(commands.HelpCommand):
    def __init__(self):
        super().__init__()

    @staticmethod
    async def split_into_chunks(lst, n) -> typing.List[typing.Any]:
        return_list = []
        for i in range(0, len(lst), n):
            return_list.append(lst[i : i + n])
        return return_list

    def get_command_signature(self, command: commands.Command) -> str:
        return "%s%s%s %s" % (
            self.clean_prefix,
            f"{command.parent} " if command.parent else "",
            command.name,
            command.signature,
        )

    def get_command_aliases(self, command: commands.Command) -> str:
        return ", ".join([f"`{x}`" for x in command.aliases])

    def format_command_aliases(self, command: commands.Command) -> str:
        aliases = self.get_command_aliases(command)
        if aliases.strip():
            return f"**Aliases**: {aliases}"
        else:
            return ""

    async def send_bot_help(self, mapping: typing.Dict[commands.Cog, typing.List[commands.Command]]):
        ctx: commands.Context = self.context
        prefix = await ctx.bot.get_prefix(ctx.guild)

        pages = []
        all_cogs = [i for i in mapping.keys() if i is not None]
        splits = 5
        chunks = await self.split_into_chunks(
            [i for i in all_cogs if i.qualified_name.lower() not in ctx.bot.config["hidden_cogs"]],
            splits,
        )  # Dividing cogs into multiple chunks

        for index, chunk in enumerate(chunks, start=1):

            em = discord.Embed(
                timestamp=datetime.now().astimezone(),
                color=discord.Color.blue(),
            )
            if index == 1:
                em.description = "Xenial is a simple yet effective Discord bot which suits your server's needs. It is packed with features randing from information commands to starboard."
            em.set_author(name=ctx.author, icon_url=ctx.author.avatar_url)
            em.set_thumbnail(url=ctx.bot.user.avatar_url)
            em.set_footer(text=ctx.bot.user, icon_url=ctx.bot.user.avatar_url)

            for cog in chunk:
                if not cog:
                    continue  # Sometimes the last cog in the iterator is None
                if cog.qualified_name.lower() in ctx.bot.config["hidden_cogs"]:
                    continue  # Don't wanna show hidden cogs

                em.add_field(
                    name=f"{cog._cog_emoji} {cog.qualified_name}",
                    value=f"{cog.description}\nRun `{prefix}help {cog.qualified_name}` for more info",
                    inline=False,
                )
            if len(chunks) > 1:
                em.title = f"Xenial Help [Page {index} of {len(chunks)}]"
            else:
                # If there is only one page, no need to paginate
                em.title = "Xenial Help"
                return await self.get_destination().send(embed=em)

            pages.append(em)

        paginator = DiscordUtils.Pagination.AutoEmbedPaginator(ctx, remove_reactions=True)
        await paginator.run(embeds=pages)

    async def send_cog_help(self, cog: commands.Cog):
        ctx: commands.Context = self.context

        _commands: typing.List[commands.Command] = cog.get_commands()

        pages = []
        splits = 5
        chunks = await self.split_into_chunks(_commands, splits)  # Dividing cogs into multiple chunks
        for index, chunk in enumerate(chunks, start=1):
            em = discord.Embed(
                timestamp=datetime.now().astimezone(),
                color=discord.Color.magenta(),
            )
            em.set_author(name=ctx.author, icon_url=ctx.author.avatar_url)
            em.set_thumbnail(url=ctx.bot.user.avatar_url)
            em.set_footer(text=ctx.bot.user, icon_url=ctx.bot.user.avatar_url)
            for command in chunk:
                em.add_field(
                    name=f"`{self.get_command_signature(command).strip()}`",
                    value=self.format_command_aliases(command) + "\n" + command.help,
                    inline=False,
                )
                if isinstance(command, commands.Group):
                    for subcommand in command.walk_commands():
                        em.add_field(
                            name=f"`{self.get_command_signature(subcommand).strip()}`",
                            value=self.format_command_aliases(subcommand) + "\n" + subcommand.help,
                            inline=False,
                        )
            if len(chunks) > 1:
                em.title = f"Xenial Help - {cog._cog_emoji} {cog.qualified_name} [Page {index} of {len(chunks)}]"
            else:
                # If there is only one page, no need to paginate
                em.title = f"Xenial Help - {cog._cog_emoji} {cog.qualified_name}"
                return await self.get_destination().send(embed=em)

            pages.append(em)

        paginator = DiscordUtils.Pagination.AutoEmbedPaginator(ctx, remove_reactions=True)
        await paginator.run(embeds=pages)

    async def send_group_help(self, group: commands.Group):
        ctx: commands.Context = self.context

        em = discord.Embed(title=f"Help for {group.name.capitalize()}", color=discord.Color.green())
        em.set_author(name=ctx.author, icon_url=ctx.author.avatar_url)
        em.set_footer(text=ctx.bot.user, icon_url=ctx.bot.user.avatar_url)
        em.timestamp = datetime.now().astimezone()

        for subcommand in group.walk_commands():
            em.add_field(
                name=f"`{self.get_command_signature(subcommand).strip()}`",
                value=self.format_command_aliases(subcommand) + "\n" + subcommand.help,
                inline=False,
            )

        return await self.get_destination().send(embed=em)

    async def send_command_help(self, command: commands.Command):
        ctx: commands.Context = self.context

        em = discord.Embed(title=f"Help for {command.name.capitalize()}", color=discord.Color.green())

        em.add_field(
            name="Name",
            value=f"{command.parent.name} {command.name}" if command.parent else command.name,
        )
        em.add_field(
            name="Category",
            value="N/A" if command.cog is None else command.cog.qualified_name,
        )
        em.add_field(name="Description", value=command.help, inline=False)
        em.add_field(
            name="Usage",
            value=f"`{self.get_command_signature(command).strip()}`",
            inline=True,
        )

        em.set_author(name=ctx.author, icon_url=ctx.author.avatar_url)
        em.set_footer(text=ctx.bot.user, icon_url=ctx.bot.user.avatar_url)
        em.timestamp = datetime.now().astimezone()

        return await self.get_destination().send(embed=em)
