import attr


@attr.s
class Uptime:
    days: int = attr.ib()
    hours: int = attr.ib()
    minutes: int = attr.ib()
    seconds: int = attr.ib()

    def __str__(self):
        if self.days:
            time_format = f"{self.days} days, {self.hours} hours, {self.minutes} minutes, and {self.seconds} seconds."
        elif self.hours:
            time_format = f"{self.hours} hours, {self.minutes} minutes, and {self.seconds} seconds."
        elif self.minutes:
            time_format = f"{self.minutes} minutes, and {self.seconds} seconds."
        else:
            time_format = f"{self.seconds} seconds."
        return time_format


def is_int(inp: str):
    try:
        int(inp)
        return True
    except TypeError:
        return False
