def is_int(inp: str):
    try:
        int(inp)
        return True
    except TypeError:
        return False


def meaningful_bool(_bool: bool):
    return "Yes" if _bool else "No"
